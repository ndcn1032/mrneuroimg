
## Overview

Accompanying code for the examples in our paper [Causal Inference on Neuroimaging Data with Mendelian Randomisation](https://doi.org/10.1016/j.neuroimage.2022.119385) (NeuroImage 2022) by B. Taschler, S. M. Smith, and T. E. Nichols.

**Abstract**

*While population-scale neuroimaging studies offer the promise of discovery and characterisation of subtle risk factors, massive sample sizes increase the power for both meaningful associations and those attributable to confounds. This motivates the need for causal modelling of observational data that goes beyond statements of association and towards deeper understanding of complex relationships between individual traits and phenotypes, clinical biomarkers, genetic variation, and brain-related measures of health. Mendelian randomisation (MR) presents a way to obtain causal inference on the basis of genetic data and explicit assumptions about the relationship between genetic variables, exposure and outcome. In this work, we provide an introduction to and overview of causal inference methods based on Mendelian randomisation, with examples involving imaging-derived phenotypes from UK Biobank to make these methods accessible to neuroimaging researchers. We motivate the use of MR techniques, lay out the underlying assumptions, introduce common MR methods and focus on several scenarios in which modelling assumptions are potentially violated, resulting in biased effect estimates. Importantly, we give a detailed account of necessary steps to increase the reliability of MR results with rigorous sensitivity analyses.*

<br>

## Example applications

The `examples` directory contains R code to run each of the 3 example application presented in the paper, involving

(1) blood pressure,

(2) bone mineral density,

(3) a cognitive trait,

and various imaging-derived phenotypes (IDPs).

Code for useful helper functions is included in the `src` directory.


<br>

## Data

Results from the bi-directional MR screening analysis across 1280 exposure--outcome pairs are listed in the supplementary `data` directory.

<br>

## Useful resources

- [MR Dictionary](https://mr-dictionary.mrcieu.ac.uk/)
- [IEU Open GWAS project](https://gwas.mrcieu.ac.uk/)
- Burgess et al. (2020). [Guidelines for performing Mendelian randomization investigations](https://doi.org/10.12688/wellcomeopenres.15555.1). Wellcome Open Research, 4, 186.
- Davey Smith et al. (2019). [STROBE-MR: Guidelines for strengthening the reporting of Mendelian randomization studies](https://doi.org/10.7287/peerj.preprints.27857b). PeerJ Preprints, 7:e27857v1.


### (Some) MR methods packages in R

- [TwoSampleMR](https://mrcieu.github.io/TwoSampleMR/)
- [MendelianRandomization](https://cran.r-project.org/web/packages/MendelianRandomization/index.html)
- [MR-PRESSO](https://github.com/rondolab/MR-PRESSO)
- [MRMix](https://github.com/gqi/MRMix)
- [mr.raps](https://github.com/qingyuanzhao/mr.raps)
- [RadialMR](https://github.com/WSpiller/RadialMR)
- [MVMR](https://github.com/WSpiller/MVMR)
- [CAUSE](https://github.com/jean997/cause)
- [nlmr](https://github.com/jrs95/nlmr)
- [SUMnlmr](https://github.com/amymariemason/SUMnlmr)
- [BayesMR](https://github.com/igbucur/BayesMR)
- [MR-GENIUS](https://github.com/bluosun/MR-GENIUS)
- [GRAPPLE](https://github.com/jingshuw/GRAPPLE)
- [MR-TRYX](https://github.com/explodecomputer/tryx)
