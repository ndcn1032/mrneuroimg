
# main analysis script involving blood pressure (as exposure)
# based on results from screening search
# incl. MR and sensitivity analyses



### preliminaries
library(TwoSampleMR)    #needs to be installed from github: https://github.com/MRCIEU/TwoSampleMR
library(MRInstruments)  #devtools::install_github("MRCIEU/MRInstruments")
library(MendelianRandomization)
library(mr.raps)
library(MRMix)
library(MRPRESSO)
library(tidyverse)
library(ggplot2)

source('./src/helper_funcs.R')



### ---- get data ----

## get available traits via API and filter by parameters of interest
gwas_all <- TwoSampleMR::available_outcomes() %>% filter(population == 'European',
                                                         sample_size > 1e3)    #note: larger cutoff excludes all IDPs
print(gwas_all, n = 20, width = Inf)



## (1) nIDP: systolic & diastolic blood pressure
label_nidp_arr <- c('systolic', 'diastolic')


## (2) IDPs: hand selected
## note: external capsule and WMHvol are more strongly associated with systolic BP,
##       SLF is more strongly associated with diastolic BP
label_idp_arr <- c('IDP_dMRI_TBSS_MD_External_capsule_R',
                   'IDP_dMRI_TBSS_MD_External_capsule_L',
                   'IDP_dMRI_TBSS_L3_External_capsule_R',
                   'IDP_T2_FLAIR_BIANCA_WMH_volume',
                   'IDP_dMRI_TBSS_MD_Superior_longitudinal_fasciculus_R',
                   'IDP_dMRI_TBSS_MD_Superior_longitudinal_fasciculus_L',
                   'IDP_dMRI_TBSS_L1_Superior_longitudinal_fasciculus_L')

## preallocation of overall results object
res_nested <- vector(mode = 'list', length = length(label_nidp_arr))
names(res_nested) <- label_nidp_arr

## output parameters
fid_out <- file.path('results', 'results_full_BP_v1.rds')


for(label_nidp in label_nidp_arr){
  cat('\n --- nIDP: ', label_nidp, ' --- \n')
  if(file.exists(fid_out)){ res_nested <- readRDS(fid_out) }

  ## preallocation
  if(is.null(res_nested[[label_nidp]])){
    res_nested[[label_nidp]] <- vector(mode = 'list', length = length(label_idp_arr))
    names(res_nested[[label_nidp]]) <- label_idp_arr
  }

  ## nIDP: get GWAS info for current nIDP
  gwas_nidp_arr <- gwas_all[grepl(label_nidp, gwas_all$trait, ignore.case = TRUE), ] %>%
                    arrange(desc(sample_size)) %>% head(n = 3)     #select GWAS with largest sample size
  gwas_nidp <- gwas_nidp_arr[1, ]    #(a) Int consortium of blood pressure (750k)
  #gwas_nidp <- gwas_nidp_arr[2, ]    #(b) UKB (430k)                             #TODO: run on both GWAS datasets (?)


  ## loop over nIDP-IDP pairs
  for(label_idp in label_idp_arr){
    cat(' --- IDP: ', label_idp, ' --- \n')
    if(!is.null(res_nested[[label_nidp]][[label_idp]]$fit$reverse)){ next }

    ### ---- preprocessing steps ----
    ## get GWAS data from BIG40 server
    ## standard SNP selection thresholds
    mr_data <- get_GWASdata(label_idp = label_idp,
                            gwas_nidp = gwas_nidp,
                            forward = TRUE, reverse = TRUE,
                            threshold_pval_forward_exposure = 5e-8,
                            threshold_pval_reverse_exposure = 5e-6)

    ## stricter SNP selection threshold (association with exposure)
    mr_data_strict <- get_GWASdata(label_idp = label_idp,
                                   gwas_nidp = gwas_nidp,
                                   forward = TRUE, reverse = FALSE,
                                   threshold_pval_forward_exposure = 5e-12)

    ## preallocation
    mr_fit <- vector(mode = 'list')
    mr_sensitivity <- vector(mode = 'list')



    ### ---- main MR analysis ----
    ## (4a) forward MR
    # mr_fit <- TwoSampleMR::mr(dat, method_list = mr_method_list()$obj)   #run all methods
    # mr_fit$forward <- TwoSampleMR::mr(mr_data$forward$dat,
    #                                   method_list = subset(mr_method_list(), use_by_default)$obj)   #run subset of most common methods
    cat('\n\n run MR methods (forward MR) ... \n')
    mr_fit$forward <- TwoSampleMR::mr(mr_data$forward$dat,
                                      method_list = c(subset(mr_method_list(), use_by_default)$obj,
                                                      'mr_simple_median',
                                                      'mr_ivw_mre',
                                                      'mr_ivw_fe',
                                                      'mr_egger_regression_bootstrap',
                                                      'mr_two_sample_ml'))                          #run subset of selected methods

    mr_fit$forward_strict <- TwoSampleMR::mr(mr_data_strict$forward$dat,
                                             method_list = c(subset(mr_method_list(), use_by_default)$obj,
                                                             'mr_simple_median',
                                                             'mr_ivw_mre',
                                                             'mr_ivw_fe',
                                                             'mr_egger_regression_bootstrap',
                                                             'mr_two_sample_ml'))                   #run subset of selected methods


    ## (4b) reverse MR
    cat('\n run MR methods (reverse MR) ... \n')
    mr_fit$reverse <- TwoSampleMR::mr(mr_data$reverse$dat,
                                      method_list = c(subset(mr_method_list(), use_by_default)$obj,
                                                      'mr_simple_median',
                                                      'mr_ivw_mre',
                                                      'mr_ivw_fe',
                                                      'mr_egger_regression_bootstrap',
                                                      'mr_two_sample_ml'))                          #run subset of selected methods


    ### ---- sensitivity analysis ----
    cat('\n run sensitivity analysis (forward MR) ... \n')
    mr_sensitivity$forward <- run_sensAnalysis(mr_data$forward$dat)

    #cat('\n run sensitivity analysis (forward MR - strict) ... \n')
    #mr_sensitivity$forward_strict <- run_sensAnalysis(mr_data_strict$forward$dat)

    cat('\n run sensitivity analysis (reverse MR) ... \n')
    mr_sensitivity$reverse <- run_sensAnalysis(mr_data$reverse$dat)



    ## ---- save output ----
    curr_out <- list('data' = mr_data,
                     'fit' = mr_fit,
                     'sensitivity' = mr_sensitivity)

    res_nested[[label_nidp]][[label_idp]] <- curr_out
    saveRDS(res_nested, file = fid_out)
  }
}


### plotting
source('./src/helper_funcs.R')

fid_out <- file.path('results', 'results_full_BP_v1.rds')
res <- readRDS(fid_out)

## multiple IDPs - estimates
plot_effectEstimatesMR(res$systolic[c(1,3,4,6,7)], breaks = seq(0, 0.04, 0.01), xlims = c(-0.005, 0.045))
plot_effectEstimatesMR(res$systolic[c(1,3,4,6,7)], direction = 'reverse',
                       breaks = seq(-5, 5, 2.5), xlims = c(-5, 5))

## select IDP
rr <- res$systolic$IDP_dMRI_TBSS_MD_External_capsule_R
direction <- 'forward'   #forward or reverse
direction <- 'reverse'

## scatter plot
mr_scatter_plot_MOD(rr$fit[[direction]], rr$data[[direction]]$dat,
                    xbreaks = seq(0, 1.25, 0.25), xlims = c(0, 1.3))
plot_standardMR(rr, direction = direction)

## MR-MIX
plot(rr$sensitivity[[direction]]$mr_mix$profile_mat[,"theta"],
     rr$sensitivity[[direction]]$mr_mix$profile_mat[,"pi0"],
     xlab = "theta", ylab = "pi0", type = "l", main = 'MR-Mix output')
abline(v = rr$sensitivity[[direction]]$mr_mix$theta, lty = 2, col = "red")


## Single-SNP forest plot
mr_forest_plot(rr$sensitivity[[direction]]$mr_single)

## LOO forest plot
mr_leaveoneout_plot(rr$sensitivity[[direction]]$mr_loo)



### ---- SNP info ----

cat('\n ---- example SNP info: phenoscanner ---- \n\n')
print(MendelianRandomization::phenoscanner(snpquery = mr_single$SNP[1:3]))

# cat('\n ---- example SNP info: BSgenome ---- \n\n')
# snps_lib <- SNPlocs.Hsapiens.dbSNP144.GRCh38
# #BSgenome::snpcount(snps_lib)   #number of SNPs across all chromosomes
# print(BSgenome::snpsById(snps_lib, snp_rs, ifnotfound = 'drop'))   #look up integer position of SNP

cat('\n ---- example SNP info: rsnps (annotations) ---- \n\n')
## get meta-info on publications + trait and p-val
## note: install dev branch that fixes new NCBI interface issues:
##       install_github('ropensci/rsnps', ref = 'new.ncbi.format.second.attempt')
print(rsnps::annotations(snp = mr_single$SNP[5], output = 'all'))

cat('\n ---- example SNP info: rsnps (NBCI query) ---- \n\n')
## get gene, chromosome and allele info on SNPs
print(rsnps::ncbi_snp_query(mr_single$SNP[1:5]))   #note: if any snp in the list is not found, it returns an error



### ---- MR plots ----

### plots
mr_scatter_plot(mr_fit, dat)

mr_leaveoneout_plot(mr_loo)

mr_forest_plot(singlesnp_results = mr_single, exponentiate = FALSE)
mr_density_plot(singlesnp_results = mr_single, mr_results = mr_fit, exponentiate = FALSE)
mr_funnel_plot(singlesnp_results = mr_single)


plot(mr_mrmix$profile_mat[,"theta"], mr_mrmix$profile_mat[,"pi0"],
     xlab = "theta", ylab = "pi0", type = "l", main = 'MR-Mix output')
abline(v = mr_mrmix$theta, lty = 2, col = "red")


mr_fit_raps <- mr.raps::mr.raps(b_exp = mr_data$forward$dat$beta.exposure,
                                b_out = mr_data$forward$ddat$beta.outcome,
                                se_exp = mr_data$forward$ddat$se.exposure,
                                se_out = mr_data$forward$ddat$se.outcome,
                                diagnosis = TRUE)
